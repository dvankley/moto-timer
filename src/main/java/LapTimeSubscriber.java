/**
 * Created by dvankley on 3/3/16.
 */
public interface LapTimeSubscriber {
    void handleNewLapTime(long lapTimeNanos, String formattedLapTime);
}
