import com.fasterxml.jackson.jr.ob.JSON;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by dvankley on 3/2/16.
 */
public class WebClientManager implements LapTimeSubscriber {
    public final String heartbeatUrl = "http://pardotmotogp.herokuapp.com/timerHealth";
    public final String newLapUrl = "http://pardotmotogp.herokuapp.com/laps.json";
    public final int heartbeatIntervalMillis = 2000;
    public final ScheduledExecutorService heartbeatExecutor = Executors.newSingleThreadScheduledExecutor();
    public final HardwareManager hardware;

    public WebClientManager(HardwareManager hardware) {
        this.hardware = hardware;

        // Spin up heartbeat task
        this.heartbeatExecutor.scheduleAtFixedRate(() -> {
            HardwareManager.Health health = this.hardware.getSensorState();
            String healthString = this.hardware.getHealthString(health);
//            motoTimer.logger.info("Sending heartbeat state: " + healthString);
//            try {
//                String response = Request.Post(this.heartbeatUrl)
//                        .bodyForm(Form.form()
//                                .add("health", healthString)
//                                .build())
//                        .execute()
//                        .returnContent()
//                        .asString();
//                motoTimer.logger.info("Heartbeat post response: " + response);
//            } catch (Exception e) {
//                motoTimer.logger.severe(e.toString());
//            }
        }, 0, this.heartbeatIntervalMillis, TimeUnit.MILLISECONDS);
    }

    @Override
    public void handleNewLapTime(long lapTimeNanos, String formattedLapTime) {
        Runnable postTask = () -> {
            try {
                String json = JSON.std
                        .composeString()
                        .startObject()
                        .startObjectField("lap")
                        .put("formatted_time", formattedLapTime)
                        .put("bike_color", "Blue")
                        .end()
                        .end()
                        .finish();

//                motoTimer.logger.info("Posting new lap time: " + json);
                String response = Request.Post(this.newLapUrl)
                        .bodyString(json, ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString();
//                motoTimer.logger.info("New lap time post response: " + response);
            } catch (Exception e) {
                motoTimer.logger.severe(e.toString());
            }
        };
        Thread thread = new Thread(postTask);
        thread.start();
    }

    public void shutdown() {
        this.heartbeatExecutor.shutdown();
    }
}
