/**
 * Created by dvankley on 2/25/16.
 */
import java.io.IOException;
import java.util.logging.*;

import spark.Spark;

public class motoTimer {
    public static final Environment env = Environment.PROD;
    public static Logger logger;

    public static void main(String[] args) {
        Spark.get("/hello", (req, res) -> "Hello World");

        motoTimer.logger = Logger.getLogger("moto-logger");
        SimpleFormatter formatter = new SimpleFormatter();

        try {
            FileHandler fh = new FileHandler("moto-timer.log");
            logger.addHandler(fh);
            fh.setFormatter(formatter);

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }

        logger.info("Main logger initialized");

        // Build managers
        HardwareManager hardware = new HardwareManager();
        WebClientManager client = new WebClientManager(hardware);
        hardware.addSubscriber(client);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            motoTimer.logger.info("Shutting down moto timer");
            client.shutdown();
        }));
    }
}
