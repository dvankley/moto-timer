/**
 * Created by dvankley on 3/2/16.
 */

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.ArrayList;
import java.util.List;

public class HardwareManager implements GpioPinListenerDigital {
    enum Health { MISALIGNED, DETECTED, CLEAR }

    private final GpioController gpio;
    private final GpioPinDigitalInput sensorInput;
    private final GpioPinDigitalOutput redLed;
    private final GpioPinDigitalOutput greenLed;

    // Five seconds
    private final long debounceThreshold = 5000000000L;
    private final long misalignedThreshold = 5000000000L;
    private long lastBreakTime = 0;
    private long lastLapNanos = 0;
    private long lastLowToHigh = 0;
    private long lastHighToLow = 0;
    private List<LapTimeSubscriber> subscribers = new ArrayList<>();

    private final long lapUpperBoundNanos = 120000000000L;
    private final long lapLowerBoundNanos = 15000000000L;

    HardwareManager() {
        motoTimer.logger.info("Initializing hardware manager...");
        if (motoTimer.env == Environment.DEV) {
            this.sensorInput = null;
            this.gpio = null;
            this.redLed = null;
            this.greenLed = null;
            return;
        }
        this.gpio = GpioFactory.getInstance();
        this.sensorInput = this.gpio.provisionDigitalInputPin(
                // PIN NUMBER
                RaspiPin.GPIO_00,
                // PIN FRIENDLY NAME (optional)
                "SensorInput",
                // PIN RESISTANCE (optional)
                PinPullResistance.PULL_UP
        );
        // Listen for digital input changes
        this.sensorInput.addListener(this);
        this.redLed = this.gpio.provisionDigitalOutputPin(
                RaspiPin.GPIO_02,
                "RedLED",
                PinState.LOW
        );
        this.greenLed = this.gpio.provisionDigitalOutputPin(
                RaspiPin.GPIO_03,
                "GreenLED",
                PinState.LOW
        );
    }

    @Override
    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
        if (event.getState() == PinState.LOW) {
            // Always log transition times
            this.lastHighToLow = System.nanoTime();
//            motoTimer.logger.info("high to low detected");

            // Calculate lap time
            long lastLap = this.lastHighToLow - this.lastBreakTime;

            // Breaks always set LED to red
            this.red();

            // If the last lap was impossibly fast, call it a bounce and ignore it
            if (lastLap < this.debounceThreshold) {
                return;
            }

            // Update timestamps
            this.lastLapNanos = lastLap;
            this.lastBreakTime = this.lastHighToLow;
            String formattedTime = this.formatLapTime(this.lastLapNanos);

            // Output the lap time to wherever
            motoTimer.logger.info(String.format("Lap detected: %s seconds", formattedTime));
            if (this.lastLapNanos > this.lapLowerBoundNanos && this.lastLapNanos < this.lapUpperBoundNanos) {
                for (LapTimeSubscriber subscriber: subscribers) {
                    subscriber.handleNewLapTime(this.lastLapNanos, formattedTime);
                }
            }
        } else {
            // Always log transition times
            this.lastLowToHigh = System.nanoTime();
//            motoTimer.logger.info("low to high detected");
        }
    }

    private void red() {
        this.redLed.high();
        this.greenLed.low();
    }

    private void green() {
        this.redLed.low();
        this.greenLed.high();
    }

    void addSubscriber(LapTimeSubscriber subscriber) {
        this.subscribers.add(subscriber);
    }

    Health getSensorState() {
        if (motoTimer.env == Environment.PROD) {
            PinState current = this.sensorInput.getState();
            // If the beam is broken now and...
            if (current == PinState.LOW) {
                this.red();
                // it's been a while since it wasn't broken, it's misaligned
                if (System.nanoTime() - this.lastHighToLow > this.misalignedThreshold) {
                    return Health.MISALIGNED;
                } else {
                    // Otherwise it's just broken for now
                    return Health.DETECTED;
                }
            }
        }
        // Only green up if we're outside of debounce
        if (System.nanoTime() - this.lastBreakTime > this.debounceThreshold) {
            this.green();
        }
        return Health.CLEAR;
    }

    String getHealthString(Health health) {
        switch (health) {
            case CLEAR:
                return "clear";
            case MISALIGNED:
                return "misaligned";
            case DETECTED:
                return "detected";
            default:
                return "undefined_health";
        }
    }

    String formatLapTime(long lapTime) {
        String integer = String.valueOf(lapTime / 1000000000L);
        String decimal = String.valueOf(lapTime % 1000000000L);
        return integer + "." + decimal;
    }
}
